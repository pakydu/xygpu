#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <module.h>


/**
 * @file xygpu_kernel_linux.c
 * Implementation of the Linux device driver entrypoints
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/compat.h>
#include <system/device/pci/pciBus.h>
#include <system/device/pci/pciMsi.h>
#include <system/device/pci/pciLib.h>
#include <system/device/pci/pciIds.h>
#include <system/device/pci/pciDev.h>

// local head files:
#include "xygpu_uk_types.h"
#include "xygpu_kernel_common.h"
#include "xygpu_osk.h"
#include "xygpu_kernel_linux.h"
#include "xygpu_covert2sylix.h"
#include "xygpu_kernel_core.h"



/* Module parameter to control log level */
int xygpu_debug_level = -1;
//module_param(xygpu_debug_level, int, S_IRUSR | S_IWUSR | S_IWGRP | S_IRGRP | S_IROTH); /* rw-rw-r-- */
//MODULE_PARM_DESC(xygpu_debug_level, "Higher number, more dmesg output");

/* By default the module uses any available major, but it's possible to set it at load time to a specific number */
//int xygpu_major = 0;
int xygpu_major = 0;
//module_param(xygpu_major, int, S_IRUGO); /* r--r--r-- */
//MODULE_PARM_DESC(xygpu_major, "Device major number");

int xygpu_benchmark = 0;
//module_param(xygpu_benchmark, int, S_IRUSR | S_IWUSR | S_IWGRP | S_IRGRP | S_IROTH); /* rw-rw-r-- */
//MODULE_PARM_DESC(xygpu_benchmark, "Bypass GA hardware when non-zero");

extern int xygpu_hang_check_interval;
//module_param(xygpu_hang_check_interval, int, S_IRUSR | S_IWUSR | S_IWGRP | S_IRGRP | S_IROTH);
//MODULE_PARM_DESC(xygpu_hang_check_interval, "Interval at which to check for progress after the hw watchdog has been triggered");

extern int xygpu_max_job_runtime;
//module_param(xygpu_max_job_runtime, int, S_IRUSR | S_IWUSR | S_IWGRP | S_IRGRP | S_IROTH);
//MODULE_PARM_DESC(xygpu_max_job_runtime, "Maximum allowed job runtime in msecs.\nJobs will be killed after this no matter what");

struct xygpu_dev
{
    LW_DEV_HDR cdev;
#if XYGPU_LICENSE_IS_GPL
    struct class *  xygpu_class;
#endif
};

int map_errcode( _xygpu_osk_errcode_t err );

static char xygpu_dev_name[] = "xygpu"; /* should be const, but the functions we call requires non-cost */

struct xygpu_device *xygpu_dev2 = NULL;

struct xygpu_asic_dev_info xygpu_glb_asic_dev_info[XYGPU_TYPE_MAX] = {
    {XYGPU_FPGA_DEV_INFO},
    {XYGPU_FPGA2_DEV_INFO},
    {XYGPU_01_DEV_INFO},
};

/* the xygpu device */
int drv_index;
static struct xygpu_dev device;

//file_opt funcs:
static long xygpu_open(LW_DEV_HDR *dev, char *name, int flag, int mode);
//static int xygpu_close(LW_DEV_HDR *dev);
static int xygpu_release(PLW_DEV_HDR *dev,    PCHAR     pcName);
static int xygpu_ioctl(LW_DEV_HDR *dev, unsigned int cmd, unsigned long arg);
static int xygpu_mmap(LW_DEV_HDR *dev, PLW_DEV_MMAP_AREA  vma);

/* Linux char file operations provided by the GA module */
struct file_operations xygpu_fops =
{
    .owner = THIS_MODULE,
    .fo_open = xygpu_open,
//    .fo_close = xygpu_close,
    .fo_release = xygpu_release,
    .fo_ioctl = xygpu_ioctl,
    .fo_mmap = xygpu_mmap
};

        //.class = (PCI_CLASS_COMMUNICATION_SERIAL << 8),
static PCI_DEV_ID_CB xygpu_pcie_ids[] = {
    {
        PCI_DEVICE(XYGPU_FPGA_PCI_VENDOR_ID,
               XYGPU_FPGA_PCI_DEVICE_ID),
        .PCIDEVID_uiSubVendor = XYGPU_FPGA_SUBSYS_VENDOR_ID,
        .PCIDEVID_uiSubDevice = XYGPU_FPGA_SUBSYS_DEVICE_ID,
        .PCIDEVID_uiClass = (PCI_CLASS_COMMUNICATION_SERIAL << 8),
        .PCIDEVID_uiClassMask = 0xffff00,
        .PCIDEVID_ulData = XYGPU_FPGA,
    },
    {
        PCI_DEVICE(XYGPU_FPGA2_PCI_VENDOR_ID,
               XYGPU_FPGA2_PCI_DEVICE_ID),
        .PCIDEVID_uiSubVendor = XYGPU_FPGA2_SUBSYS_VENDOR_ID,
        .PCIDEVID_uiSubDevice = XYGPU_FPGA2_SUBSYS_DEVICE_ID,
        .PCIDEVID_uiClass = (PCI_CLASS_DISPLAY_VGA << 8),
        .PCIDEVID_uiClassMask = 0xffff00,
        .PCIDEVID_ulData = XYGPU_FPGA2,
    },
    {
        PCI_DEVICE(XYGPU_PCI_VENDOR_ID,
               XYGPU_PCI_DEVICE_ID),
        .PCIDEVID_uiClass = (PCI_CLASS_DISPLAY_VGA << 8),
        .PCIDEVID_uiClassMask = 0xffff00,
        .PCIDEVID_ulData = XYGPU_01,
    },
    { }
};

static struct xygpu_device *to_xygpu_device(PCI_DEV_HANDLE dev) {
    struct xygpu_device *xygpu;
    xygpu = xygpu_dev2;
    return xygpu;
}

static inline void _reg_write(u32 val, void __iomem *regbase, u32 offset) {
#ifndef __KERNEL__
    write32(val, (addr_t)(regbase+offset));
#else
    iowrite32(val, regbase+offset);
#endif
}

static inline u32 _reg_read(void __iomem *regbase, u32 offset) {
#ifndef __KERNEL__
    return read32((addr_t)(regbase+offset));
#else
    return ioread32(regbase+offset);  //write32(value, (addr_t)reg);
#endif
}

static irqreturn_t pci_driver_isr (void *arg, unsigned long vector)
{
//    UINT32  regInfo = PCI_NONE_READ(arg, 0);
//    if (!regInfo) {
//        return  (LW_IRQ_NONE);
//    }

    return  (LW_IRQ_HANDLED);
}


static int xygpu_pcie_probe(PCI_DEV_HANDLE pdev, const PCI_DEV_ID_HANDLE pci_id)
{
    int ret, i;
    struct xygpu_pcie_info *xygpu_pcie_priv;
    ULONG                   irqVector;
    PCI_RESOURCE_HANDLE     hResource;
    printk(KERN_INFO "%s: start\n", __func__);

    _xygpu_osk_errcode_t err;


    enum xygpu_asic_type xygpu_type = (enum xygpu_asic_type)pci_id->PCIDEVID_ulData;
    //struct xygpu_asic_dev_info *xygpu_dev_info = xygpu_get_dev_ifo(xygpu_type);
    struct xygpu_asic_dev_info *xygpu_dev_info = &xygpu_glb_asic_dev_info[xygpu_type];

    xygpu_pcie_priv = (struct xygpu_pcie_info *)kmalloc(sizeof(struct xygpu_pcie_info), GFP_KERNEL);
    xygpu_pcie_priv->pdev = pdev;
    xygpu_pcie_priv->xygpu_dev_info = xygpu_dev_info;

    printk(KERN_INFO "%s: set master\n", __func__);
    pciDevMasterEnable(pdev, LW_TRUE);

    printk(KERN_INFO "%s: trying to enable MSIx\n", __func__);
    pciDevMsiEnableSet(pdev, LW_TRUE);
    ret = pciDevMsiRangeEnable(pdev, 1, 6);
    irqVector = pdev->PCIDEV_ulDevIrqVector;
    printk(KERN_INFO "xygpu_pcie_probe: pci_alloc_irq ret: %d\n", ret);
    printk(KERN_INFO "xygpu_pcie_probe: hMsgHandle->irq: %d\n", pdev->PCIDEV_ulDevIrqVector);

    //enable the irq.
    pciDevInterConnect(pdev, irqVector, pci_driver_isr, LW_NULL, "gpu_dev");
    pciDevInterEnable(pdev, irqVector, pci_driver_isr, LW_NULL);

    err = xygpu_kernel_constructor(pdev);
//    err = _XYGPU_OSK_ERR_OK;
    if (_XYGPU_OSK_ERR_OK != err) {
        XYGPU_PRINT(("Failed to initialize driver (error %d)\n", err));
        //pci_unregister_driver(&xygpu_pci);
        return -EFAULT;
    }

    printk(KERN_INFO "%s: bar setup\n", __func__);
    for (i = 0; i < PCI_BAR_MAX_CNT; i++) {
        hResource = &pdev->PCIDEV_tResource[i];
        u32 uiType = PCI_RESOURCE_TYPE(hResource);
        printk(KERN_INFO "start bar[%d] resource type: %d\n", i, uiType);

        //hResource = pciDevResourceGet(pdev, PCI_IORESOURCE_MEM, i);
        if (!hResource) {
            return  (PX_ERROR);
        }
        xygpu_pcie_priv->start_bar[i] = PCI_RESOURCE_START(hResource);
        printk(KERN_INFO "start bar[%d]: 0x%llx\n", i, xygpu_pcie_priv->start_bar[i]);
        xygpu_pcie_priv->len_bar[i] = PCI_RESOURCE_SIZE(hResource);
        printk(KERN_INFO "len bar[%d]: 0x%x\n", i, xygpu_pcie_priv->len_bar[i]);

        if (xygpu_pcie_priv->len_bar[i] != 0 && i == 2) { //the bar2 is for DDR.
//            request_mem_region(xygpu_pcie_priv->start_bar[i], xygpu_pcie_priv->len_bar[i], XYGPU_PCI_NAME);
//            xygpu_pcie_priv->pci_mmio_bar[i] = ioremap_wc(xygpu_pcie_priv->start_bar[i], xygpu_pcie_priv->len_bar[i]);
            xygpu_pcie_priv->pci_mmio_bar[i] = API_PciDevIoRemapEx2(xygpu_pcie_priv->start_bar[i], xygpu_pcie_priv->len_bar[i], 0x07/*LW_VMM_FLAG_VALID|LW_VMM_FLAG_ACCESS|LW_VMM_FLAG_WRITABLE|LW_VMM_FLAG_WRITETHROUGH*/);

            if (!xygpu_pcie_priv->pci_mmio_bar[i]) {
                printk(KERN_ERR  "pci ioremap error!\n");
                return -EINVAL;
            }
            printk(KERN_INFO "ioremap_wc: pci_mmio_bar[%d]: 0x%llx\n", i, xygpu_pcie_priv->pci_mmio_bar[i]);
        } else {
//            xygpu_pcie_priv->pci_mmio_bar[i] = ioremap_nocache(xygpu_pcie_priv->start_bar[i], xygpu_pcie_priv->len_bar[i]);
            xygpu_pcie_priv->pci_mmio_bar[i] = NULL;
        }
    }

    printk(KERN_INFO "%s: dev2 1\n", __func__);
    xygpu_dev2 = (struct xygpu_device*)kmalloc(sizeof(struct xygpu_device), GFP_KERNEL);
    xygpu_dev2->pcie_info = xygpu_pcie_priv;
    xygpu_dev2->pci_dev = pdev;
//    xygpu_dev2->dev = &pdev->dev;

//    printk(KERN_INFO "xygpu_pcie_probe: dev2 2\n");
//    struct device *dev = &pdev->dev;
//    printk(KERN_INFO "xygpu_pcie_probe: dev2 3\n");
    struct xygpu_device *xygpudev = to_xygpu_device(NULL);
//    printk(KERN_INFO "xygpu_pcie_probe: dev2 4\n");

    //int irq0=pci_irq_vector(pdev, 0);
    //int irq0 = pci_alloc_irq_vectors(pdev, 1, 4, PCI_IRQ_MSIX|PCI_IRQ_AFFINITY);
    //int irqn = pci_alloc_irq_vectors(pdev, 1, 1, PCI_IRQ_MSI);
    //printk(KERN_INFO "xygpu_pcie_probe: irqn: %d\n", irqn);
    //int irq0=pci_irq_vector(pdev, 0);
    //printk(KERN_INFO "xygpu_pcie_probe: irq0: %d\n", irq0);
    //int irq1=pci_irq_vector(pdev, 1);
    //printk(KERN_INFO "xygpu_pcie_probe: irq1: %d\n", irq1);
    //int irq2=pci_irq_vector(pdev, 2);
    //printk(KERN_INFO "xygpu_pcie_probe: irq2: %d\n", irq2);
    //int irq3=pci_irq_vector(pdev, 3);
    //printk(KERN_INFO "xygpu_pcie_probe: irq3: %d\n", irq3);
    //int irq4=pci_irq_vector(pdev, 4);
    //printk(KERN_INFO "xygpu_pcie_probe: irq4: %d\n", irq4);
    //int irq5=pci_irq_vector(pdev, 5);
    //printk(KERN_INFO "xygpu_pcie_probe: irq5: %d\n", irq5);
    //int irq6=pci_irq_vector(pdev, 6);
    //printk(KERN_INFO "xygpu_pcie_probe: irq6: %d\n", irq6);
    return 0;


    if (xygpu_pcie_priv->len_bar[0] != 0) {
        void __iomem *gpu_base = xygpudev->pcie_info->pci_mmio_bar[0];
        printk(KERN_INFO "xygpu_pcie_probe: dev2 5\n");
        printk(KERN_INFO "_reg_write: 0x%llx\n", xygpudev->pcie_info->pci_mmio_bar[0]);
        //xygpu_pcie_priv->pci_mmio_bar[i];
        // _xygpu_reg_write(gpu_base, REG_OFFSET(), width)
        if (gpu_base) {
            printk(KERN_INFO "_reg_read & write: 0x%llx\n", gpu_base);
            printk(KERN_INFO "xygpu_pcie_probe: dev2 6\n");
            u32 v = _reg_read(gpu_base, 0);
            printk(KERN_INFO "_reg_read: 0x%x\n", v);
            // int j;
            // for (j=0; j<1; j++) {
            //     u32 v = _reg_read(gpu_base + j , 0);
            //     printk(KERN_INFO "_reg_read: j: %d, v: 0x%x\n", j, v);
            // }
            printk(KERN_INFO "_reg_write: 0x12345 to 0x%llx\n", gpu_base);
            _reg_write(0x12345, gpu_base, 0);
            v = _reg_read(gpu_base, 0);
            printk(KERN_INFO "_reg_read: 0x%x\n", v);
            //u32 v = _reg_read(xygpudev->pcie_info->pci_mmio_bar[0], 0);
        }
    }
    return 0;

// DDR UMP=0
// 0x00000000 - 0x01800000,   //   0M - 24M
// 0x08000000 - 0x20000000,   // 128M - 512M
//
// DDR - UMP=1
// 0x00000000 - 0x01800000,   //   0M - 24M
// 0x0A000000 - 0x20000000,   // 160M - 512M
    if (xygpu_pcie_priv->len_bar[2] != 0) {
        void __iomem *ddr_base = xygpudev->pcie_info->pci_mmio_bar[2];
        printk(KERN_INFO "xygpu_pcie_probe: dev2 5\n");
        printk(KERN_INFO "_reg_write: 0x%llx\n", xygpudev->pcie_info->pci_mmio_bar[2]);
        //xygpu_pcie_priv->pci_mmio_bar[i];
        // _xygpu_reg_write(ddr_base, REG_OFFSET(), width)
        if (ddr_base){
            printk(KERN_INFO "_reg_write: 0x%llx\n", ddr_base);
            printk(KERN_INFO "xygpu_pcie_probe: dev2 6\n");
            u32 v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: 0x%x\n", v);
            printk(KERN_INFO "_reg_write: 0x12345 to 0x%llx\n", ddr_base);
            _reg_write(0x12345, ddr_base, 0);
            v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: addr:0x%llx, v: 0x%x\n", ddr_base, v);
            //u32 v = _reg_read(xygpudev->pcie_info->pci_mmio_bar[0], 0);
            ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x800000; //8388608; // +8M
            printk(KERN_INFO "_reg_write: 0x12346 to 0x%llx\n", ddr_base);
            _reg_write(0x12346, ddr_base, 0);
            v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: addr:0x%llx, v: 0x%x\n", ddr_base, v);
            ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x20000000 -4; // +512M -4bytes
            // ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x20000000 * 4 -1; // +512M -1bytes  // KERNEL UPF
            printk(KERN_INFO "_reg_write: 0x12347 to 0x%llx\n", ddr_base);
            _reg_write(0x12347, ddr_base, 0);
            v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: addr:0x%llx, v: 0x%x\n", ddr_base, v);

            ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x01800000 - 4; // +24M
            printk(KERN_INFO "_reg_write: 0x12348 to 0x%llx\n", ddr_base);
            _reg_write(0x12348, ddr_base, 0);
            v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: addr:0x%x, v: 0x%x\n", ddr_base, v);

            ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x01800000; // +24M
            printk(KERN_INFO "_reg_write: 0x12349 to 0x%llx\n", ddr_base);
            _reg_write(0x12349, ddr_base, 0);
            v = _reg_read(ddr_base, 0);
            printk(KERN_INFO "_reg_read: addr:0x%x, v: 0x%x\n", ddr_base, v);

            u32 j;
            //for (j=0;j<0x20000000;j++){
            for (j=0;j<0x20;j++){
                ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + j; // j
                printk(KERN_INFO "_reg_write: 0x%x to 0x%llx\n", j, ddr_base);
                _reg_write(j, ddr_base, 0);
                v = _reg_read(ddr_base, 0);
                printk(KERN_INFO "_reg_read: addr:0x%x, v: 0x%x\n", ddr_base, v);
            }

            // for (j=0x20000000 - 20;j<0x20000000;j++){
            //  ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + j; // j
            //  printk(KERN_INFO "_reg_write: 0x%x to 0x%llx\n", j, ddr_base);
            //  _reg_write(j, ddr_base, 0);
            //  v = _reg_read(ddr_base, 0);
            //  printk(KERN_INFO "_reg_read: addr:0x%x, v: 0x%x\n", ddr_base, v);
            // }


            // ddr_base = xygpudev->pcie_info->pci_mmio_bar[2] + 0x20000000; // +512M  // KERNEL UPF
            // printk(KERN_INFO "_reg_write: 0x12340 to 0x%llx\n", ddr_base);
            // _reg_write(0x12340, ddr_base, 0);
            // v = _reg_read(ddr_base, 0);
            // printk(KERN_INFO "_reg_read: addr:0x%x, v: 0x%x\n", ddr_base, v);
        }
    }
    return 0;
}

static void xygpu_pcie_remove(PCI_DEV_HANDLE pci_dev)
{
}

static void xygpu_pcie_shutdown(PCI_DEV_HANDLE pci_dev)
{
}

static PCI_DRV_CB xygpu_pci = {
    .PCIDRV_cDrvName       = XYGPU_PCI_NAME,
    .PCIDRV_hDrvIdTable   = xygpu_pcie_ids,
    .PCIDRV_pfuncDevProbe      = xygpu_pcie_probe,
    .PCIDRV_pfuncDevRemove     = xygpu_pcie_remove,
    .PCIDRV_pfuncDevShutdown   = xygpu_pcie_shutdown,
};

int xygpu_driver_init(void)
{

    _xygpu_osk_errcode_t err;

    printk(KERN_INFO "%s done1\n", __func__);

    err = pciDrvRegister(&xygpu_pci);
    if (err) {
        XYGPU_PRINT(("Failed to initialize pci driver (error %d)\n", err));
        return -EINVAL;
    }

    printk(KERN_INFO "%s done2\n", __func__);


    XYGPU_PRINT(("Start to init the driver!! -- Sietium@2022\n"));

    return 0;
}

void xygpu_driver_exit(void)
{
    PCI_DRV_HANDLE xygpu_driver;
    printk(KERN_INFO "%s\n", __func__);

    printk("bye the XYGPU...");
    if (xygpu_dev2) {
        struct xygpu_pcie_info *xygpu_pcie_priv = xygpu_dev2->pcie_info;
        if (xygpu_pcie_priv) {
            int i = 0;
            printk(KERN_INFO "%s: bar free\n", __func__);
            for (i = 0; i < PCI_BAR_MAX_CNT; i++) {
                if (xygpu_pcie_priv->pci_mmio_bar[i]) {
                    API_PciDevIoUnmap(xygpu_pcie_priv->pci_mmio_bar[i]);
                }
            }
            kfree(xygpu_pcie_priv);
        }
        kfree(xygpu_dev2);
    }

    xygpu_driver = pciDrvHandleGet(XYGPU_PCI_NAME);
    pciDrvUnregister(xygpu_driver);
    printk(KERN_INFO "xygpu_driver_exit done\n");
}

/* called from _xygpu_osk_init */
int initialize_kernel_device(void)
{
    int err = 0;

    printk(KERN_INFO "initialize_kernel_device ....\n");
    memset(&device, 0, sizeof(device));

    /* initialize our char dev data */
    drv_index = API_IosDrvInstallEx(&xygpu_fops);
    if (drv_index < 0) {
        printk("driver install fail.\r\n");
        return -1;
    }

    DRIVER_LICENSE(drv_index, "GPL 2.0");
    DRIVER_AUTHOR(drv_index, "Sietium@2022");
    DRIVER_DESCRIPTION(drv_index, "xygpu driver.");

    char dev_name[128] = {0};
    sprintf(dev_name, "/dev/%s", xygpu_dev_name);
    int ret = API_IosDevAdd(&(device.cdev), dev_name, drv_index);
    if (ret != ERROR_NONE) {
        printk("device add fail.\r\n");
        API_IosDrvRemove(drv_index, TRUE);
        return -1;
    } else {
#if XYGPU_LICENSE_IS_GPL   //not supoort this in sylixos.
        printk(KERN_INFO "%s ..1..\n", __func__);
        device.xygpu_class = class_create(THIS_MODULE, xygpu_dev_name);
        if (IS_ERR(device.xygpu_class)) {
            err = PTR_ERR(device.xygpu_class);
            printk(KERN_INFO "initialize_kernel_device/ err1 ....\n");
        } else {
            struct device * mdev;
            mdev = device_create(device.xygpu_class, NULL, dev, NULL, xygpu_dev_name);
            if (!IS_ERR(mdev)) {
                return 0;
            }

            printk(KERN_INFO "initialize_kernel_device/ err2 ....\n");
            err = PTR_ERR(mdev);
        }

        printk(KERN_INFO "initialize_kernel_device/ cdev_del() ....\n");
        cdev_del(&device.cdev);
#else
        return 0;
#endif
    }
    return err;
}

/* called from _xygpu_osk_term */
void terminate_kernel_device(void)
{

#if XYGPU_LICENSE_IS_GPL
    device_destroy(device.xygpu_class, dev);
    class_destroy(device.xygpu_class);
#endif
    /* unregister char device */
    API_IosDevDelete(&(device.cdev));
    API_IosDrvRemove(drv_index, TRUE);
    return;
}

/** @note munmap handler is done by vma close handler */
static int xygpu_mmap(LW_DEV_HDR *dev, PLW_DEV_MMAP_AREA  vma)
{
    struct xygpu_session_data * session_data;
//printk(KERN_INFO "xygpu_mmap()\n");
    _xygpu_uk_mem_mmap_s args = {0, };

    session_data = (struct xygpu_session_data *)dev->DEVHDR_pvReserve;
    if (NULL == session_data)
    {
        XYGPU_PRINT_ERROR(("mmap called without any session data available\n"));
        return -EFAULT;
    }

    XYGPU_DEBUG_PRINT(3, ("MMap() handler: start=0x%lX, phys=0x%lX, size=0x%08X\n", (unsigned long)vma->DMAP_pvAddr, (unsigned long)(vma->DMAP_offPages << LW_CFG_VMM_PAGE_SHIFT), (unsigned long)(vma->DMAP_stLen)) );

    /* Re-pack the arguments that mmap() packed for us */
    args.ctx = session_data;
    args.phys_addr = vma->DMAP_offPages << LW_CFG_VMM_PAGE_SHIFT;
    args.size = vma->DMAP_stLen;
    args.ukk_private = vma;

    /* Call the common mmap handler */
//    XYGPU_CHECK(_XYGPU_OSK_ERR_OK ==_xygpu_ukk_mem_mmap( &args ), -EFAULT);

    return 0;
}

static long xygpu_open(LW_DEV_HDR *dev, char *name, int flag, int mode)
{
    struct xygpu_session_data *session_data = NULL;
    _xygpu_osk_errcode_t err = _XYGPU_OSK_ERR_OK;

    /* input validation */
    if (dev == NULL) return -ENODEV;

    /* allocated struct to track this session */
//    err = _xygpu_ukk_open((void **)&session_data);
    if (_XYGPU_OSK_ERR_OK != err) return map_errcode(err);

    /* initialize file pointer */
//    filp->f_pos = 0;

    /* link in our session data */
    dev->DEVHDR_pvReserve = (void*)session_data;

    return (long)dev;
}

static int xygpu_release(PLW_DEV_HDR *dev,    PCHAR     pcName)
{
    _xygpu_osk_errcode_t err = _XYGPU_OSK_ERR_OK;
    printk(KERN_INFO "%s: pcNmae=%s\n", __func__, pcName);

    /* input validation */
//    if (0 != MINOR(inode->i_rdev)) return -ENODEV;

//    err = _xygpu_ukk_close((void **)&(dev->DEVHDR_pvReserve));
    if (_XYGPU_OSK_ERR_OK != err) return map_errcode(err);

    return 0;
}

int map_errcode( _xygpu_osk_errcode_t err )
{
    switch(err)
    {
        case _XYGPU_OSK_ERR_OK : return 0;
        case _XYGPU_OSK_ERR_FAULT: return -EFAULT;
        case _XYGPU_OSK_ERR_INVALID_FUNC: return -ENOTTY;
        case _XYGPU_OSK_ERR_INVALID_ARGS: return -EINVAL;
        case _XYGPU_OSK_ERR_NOMEM: return -ENOMEM;
        case _XYGPU_OSK_ERR_TIMEOUT: return -ETIMEDOUT;
        case _XYGPU_OSK_ERR_RESTARTSYSCALL: return -ERESTARTSYS;
        case _XYGPU_OSK_ERR_ITEM_NOT_FOUND: return -ENOENT;
        default: return -EFAULT;
    }
}

static int xygpu_ioctl(LW_DEV_HDR *dev, unsigned int cmd, unsigned long arg)
{
    int err;
    struct xygpu_session_data *session_data;
#if 1
printk(KERN_INFO "xygpu_ioctl(), u: cmd: %u\n", cmd);
#endif

    //XYGPU_DEBUG_PRINT(7, ("Ioctl received 0x%08X 0x%08lX\n", cmd, arg));

    session_data = (struct xygpu_session_data *)dev->DEVHDR_pvReserve;
    if (NULL == session_data)
    {
        XYGPU_DEBUG_PRINT(7, ("filp->private_data was NULL\n"));
        return -ENOTTY;
    }
    if (NULL == (void *)arg)
    {
        XYGPU_DEBUG_PRINT(7, ("arg was NULL\n"));
        return -ENOTTY;
    }

    switch(cmd)
    {
//        case XYGPU_IOC_GET_SYSTEM_INFO_SIZE:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GET_SYSTEM_INFO_SIZE\n"));
//            err = get_system_info_size_wrapper(session_data, (_xygpu_uk_get_system_info_size_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GET_SYSTEM_INFO:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GET_SYSTEM_INFO\n"));
//            err = get_system_info_wrapper(session_data, (_xygpu_uk_get_system_info_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_WAIT_FOR_NOTIFICATION:
//        //XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_WAIT_FOR_NOTIFICATION\n"));
//            err = wait_for_notification_wrapper(session_data, (_xygpu_uk_wait_for_notification_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GET_API_VERSION:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GET_API_VERSION\n"));
//            err = get_api_version_wrapper(session_data, (_xygpu_uk_get_api_version_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_INIT:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_INIT\n"));
//            err = mem_init_wrapper(session_data, (_xygpu_uk_init_mem_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_TERM:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_TERM\n"));
//            err = mem_term_wrapper(session_data, (_xygpu_uk_term_mem_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_MAP_EXT:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_MAP_EXT\n"));
//            err = mem_map_ext_wrapper(session_data, (_xygpu_uk_map_external_mem_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_UNMAP_EXT:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_UNMAP_EXT\n"));
//            err = mem_unmap_ext_wrapper(session_data, (_xygpu_uk_unmap_external_mem_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_QUERY_MMU_PAGE_TABLE_DUMP_SIZE:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_QUERY_MMU_PAGE_TABLE_DUMP_SIZE\n"));
//            err = mem_query_mmu_page_table_dump_size_wrapper(session_data, (_xygpu_uk_query_mmu_page_table_dump_size_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_DUMP_MMU_PAGE_TABLE:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_DUMP_MMU_PAGE_TABLE\n"));
//            err = mem_dump_mmu_page_table_wrapper(session_data, (_xygpu_uk_dump_mmu_page_table_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_GET_BIG_BLOCK:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_GET_BIG_BLOCK\n"));
//            err = mem_get_big_block_wrapper(filp, (_xygpu_uk_get_big_block_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_FREE_BIG_BLOCK:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_FREE_BIG_BLOCK\n"));
//            err = mem_free_big_block_wrapper(session_data, (_xygpu_uk_free_big_block_s __user *)arg);
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_MEM_FREE_BIG_BLOCK DONE\n"));
//            break;
//
//#if XYGPU_USE_UNIFIED_MEMORY_PROVIDER != 0
//
//        case XYGPU_IOC_MEM_ATTACH_UMP:
//            err = mem_attach_ump_wrapper(session_data, (_xygpu_uk_attach_ump_mem_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_MEM_RELEASE_UMP:
//            err = mem_release_ump_wrapper(session_data, (_xygpu_uk_release_ump_mem_s __user *)arg);
//            break;
//
//#else
//
//        case XYGPU_IOC_MEM_ATTACH_UMP:
//        case XYGPU_IOC_MEM_RELEASE_UMP: /* FALL-THROUGH */
//            XYGPU_DEBUG_PRINT(2, ("UMP not supported\n", cmd, arg));
//            err = -ENOTTY;
//            break;
//#endif
//
//        case XYGPU_IOC_PP_START_JOB:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_PP_START_JOB\n"));
//            err = pp_start_job_wrapper(session_data, (_xygpu_uk_pp_start_job_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_PP_ABORT_JOB:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_PP_ABORT_JOB\n"));
//            err = pp_abort_job_wrapper(session_data, (_xygpu_uk_pp_abort_job_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_PP_NUMBER_OF_CORES_GET:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_PP_NUMBER_OF_CORES_GET\n"));
//            err = pp_get_number_of_cores_wrapper(session_data, (_xygpu_uk_get_pp_number_of_cores_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_PP_CORE_VERSION_GET:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_PP_CORE_VERSION_GET\n"));
//            err = pp_get_core_version_wrapper(session_data, (_xygpu_uk_get_pp_core_version_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GP2_START_JOB:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GP2_START_JOB\n"));
//            err = gp_start_job_wrapper(session_data, (_xygpu_uk_gp_start_job_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GP2_ABORT_JOB:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GP2_ABORT_JOB\n"));
//            err = gp_abort_job_wrapper(session_data, (_xygpu_uk_gp_abort_job_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GP2_NUMBER_OF_CORES_GET:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GP2_NUMBER_OF_CORES_GET\n"));
//            err = gp_get_number_of_cores_wrapper(session_data, (_xygpu_uk_get_gp_number_of_cores_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GP2_CORE_VERSION_GET:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GP2_CORE_VERSION_GET\n"));
//            err = gp_get_core_version_wrapper(session_data, (_xygpu_uk_get_gp_core_version_s __user *)arg);
//            break;
//
//        case XYGPU_IOC_GP2_SUSPEND_RESPONSE:
//        XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_GP2_SUSPEND_RESPONSE\n"));
//            err = gp_suspend_response_wrapper(session_data, (_xygpu_uk_gp_suspend_response_s __user *)arg);
//            break;
    case 200: //Just for test.
          XYGPU_DEBUG_PRINT(2, ("@@@@@@@@@@ XYGPU_IOC_TEST\n"));
          int tmp_val = 0;//*((int *)arg);
          get_user(tmp_val, (int *)arg);
          XYGPU_DEBUG_PRINT(7, ("Ioctl received 0x%08X val:%d\n", cmd, tmp_val));
          break;

        default:
            XYGPU_DEBUG_PRINT(2, ("No handler for ioctl 0x%08X 0x%08lX\n", cmd, arg));
            err = -ENOTTY;
    };

    return err;
}

/*
 *  SylixOS call module_init() and module_exit() automatically.
 */
int module_init (void)
{
    printk("xygpu_module init!\n");
    xygpu_driver_init();

    return 0;
}

void module_exit (void)
{
    printk("xygpu_module exit!\n");
    xygpu_driver_exit();
}

///*
// *  module export symbols
// */
//LW_SYMBOL_EXPORT void hello_module_func (void)
//{
//    printk("hello_module_func() run!\n");
//}
