/*
 * xygpu_osk_xygpu.c
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */


/**
 * @file xygpu_osk_xygpu.c
 * Implementation of the OS abstraction layer which is specific for the GA kernel device driver
 */
#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <module.h>


/**
 * @file xygpu_kernel_linux.c
 * Implementation of the Linux device driver entrypoints
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/compat.h>
#include <system/device/pci/pciDev.h>


#include "xygpu_kernel_common.h" /* XYGPU_xxx macros */
#include "xygpu_osk.h"           /* kernel side OS functions */
#include "xygpu_uk_types.h"
#include "xygpu_kernel_linux.h"  /* exports initialize/terminate_kernel_device() */
#include "arch/config.h"        /* contains the configuration of the arch we are compiling for */

/* is called from xygpu_kernel_constructor in common code */
_xygpu_osk_errcode_t _xygpu_osk_init( void )
{
    if (0 != initialize_kernel_device()) XYGPU_ERROR(_XYGPU_OSK_ERR_FAULT);

    XYGPU_SUCCESS;
}

/* is called from xygpu_kernel_deconstructor in common code */
void _xygpu_osk_term( void )
{
    terminate_kernel_device();
}

_xygpu_osk_errcode_t _xygpu_osk_resources_init( _xygpu_osk_resource_t **arch_config, u32 *num_resources )
{
    *num_resources = sizeof(arch_configuration) / sizeof(arch_configuration[0]);
    *arch_config = arch_configuration;
    return _XYGPU_OSK_ERR_OK;
}

void _xygpu_osk_resources_term( _xygpu_osk_resource_t **arch_config, u32 num_resources )
{
    /* Nothing to do */
}
