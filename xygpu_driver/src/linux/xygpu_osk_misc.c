/*
 * xygpu_osk_misc.c
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

/**
 * @file xygpu_osk_misc.c
 * Implementation of the OS abstraction layer for the kernel device driver
 */
#define  __SYLIXOS_KERNEL
#define  __SYLIXOS_PCI_DRV
#include <SylixOS.h>
#include <linux/kernel.h>
//#include "xygpu_osk.h"

void _xygpu_osk_dbgmsg( const char *fmt, ... )
{
//    va_list args;
//    va_start(args, fmt);
//    vprintk(fmt, args);
//    va_end(args);
    printk(fmt);
}

void _xygpu_osk_abort(void)
{
    /* make a simple fault by dereferencing a NULL pointer */
    *(int *)0 = 0;
}

void _xygpu_osk_break(void)
{
    _xygpu_osk_abort();
}
