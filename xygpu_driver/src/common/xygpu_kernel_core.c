/*
 * xygpu_kernel_core.c
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

#define  __SYLIXOS_KERNEL
#include <SylixOS.h>
#include <module.h>


/**
 * @file xygpu_kernel_linux.c
 * Implementation of the Linux device driver entrypoints
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/compat.h>
#include <system/device/pci/pciDev.h>

// local head files:
#include "xygpu_uk_types.h"
#include "xygpu_kernel_common.h"
#include "xygpu_osk.h"
#include "xygpu_osk_xygpu.h"
#include "xygpu_covert2sylix.h"
#include "xygpu_kernel_core.h"



/* platform specific set up */
#include "xygpu_platform.h"

static PCI_DEV_HANDLE pcidev = NULL;
/* is called from OS specific driver entry point */
_xygpu_osk_errcode_t xygpu_kernel_constructor( PCI_DEV_HANDLE pdev)
{
    _xygpu_osk_errcode_t err;
    pcidev = pdev;

    err = xygpu_platform_init(NULL);
    if (_XYGPU_OSK_ERR_OK != err) goto error1;

    err = _xygpu_osk_init();
    if (_XYGPU_OSK_ERR_OK != err) goto error2;

    XYGPU_DEBUG_PRINT(2, ("\n"));
    XYGPU_DEBUG_PRINT(2, ("Inserting GA v%d device driver. \n",_XYGPU_API_VERSION));
    XYGPU_DEBUG_PRINT(2, ("Compiled: %s, time: %s.\n", __DATE__, __TIME__));
//    XYGPU_DEBUG_PRINT(2, ("Svn revision: %s\n", SVN_REV_STRING));

//    err  = initialize_subsystems();
//    if (_XYGPU_OSK_ERR_OK != err) goto error3;

//    XYGPU_PRINT(("GA device driver %s loaded\n", SVN_REV_STRING));

    XYGPU_SUCCESS;

//error3:
//    XYGPU_PRINT(("GA subsystems failed\n"));
//    _xygpu_osk_term();
error2:
    XYGPU_PRINT(("GA device driver init failed\n"));
    if (_XYGPU_OSK_ERR_OK != xygpu_platform_deinit(NULL))
    {
        XYGPU_PRINT(("Failed to deinit platform\n"));
    }
error1:
    XYGPU_PRINT(("Failed to init platform\n"));
    XYGPU_ERROR(err);
}
