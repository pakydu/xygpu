/*
 * xygpu_kernel_core.h
 *
 *  Created on: Oct 18, 2022
 *      Author: paky
 */

#ifndef SRC_COMMON_XYGPU_KERNEL_CORE_H_
#define SRC_COMMON_XYGPU_KERNEL_CORE_H_


//#include <linux/pci.h>
//
//#include "xygpu_osk.h"
//
//#if USING_XYGPU_PMM
//#include "xygpu_ukk.h"
//#include "xygpu_pmm.h"
//#include "xygpu_pmm_system.h"
//#endif

_xygpu_osk_errcode_t xygpu_kernel_constructor( PCI_DEV_HANDLE pdev);
void xygpu_kernel_destructor( void );

/**
 * @brief Tranlate CPU physical to GA physical addresses.
 *
 * This function is used to convert CPU physical addresses to GA Physical
 * addresses, such that _xygpu_ukk_map_external_mem may be used to map them
 * into GA. This will be used by _xygpu_ukk_va_to_xygpu_pa.
 *
 * This function only supports physically contiguous regions.
 *
 * A default implementation is provided, which uses a registered MEM_VALIDATION
 * resource to do a static translation. Only an address range which will lie
 * in the range specified by MEM_VALIDATION will be successfully translated.
 *
 * If a more complex, or non-static translation is required, then the
 * implementor has the following options:
 * - Rewrite this function to provide such a translation
 * - Integrate the provider of the memory with UMP.
 *
 * @param[in,out] phys_base pointer to the page-aligned base address of the
 * physical range to be translated
 *
 * @param[in] size size of the address range to be translated, which must be a
 * multiple of the physical page size.
 *
 * @return on success, _XYGPU_OSK_ERR_OK and *phys_base is translated. If the
 * cpu physical address range is not in the valid range, then a suitable
 * _xygpu_osk_errcode_t error.
 *
 */
_xygpu_osk_errcode_t xygpu_kernel_core_translate_cpu_to_xygpu_phys_range( u64 *phys_base, u32 size );


/**
 * @brief Validate a GA physical address range.
 *
 * This function is used to ensure that an address range passed to
 * _xygpu_ukk_map_external_mem is allowed to be mapped into GA.
 *
 * This function only supports physically contiguous regions.
 *
 * A default implementation is provided, which uses a registered MEM_VALIDATION
 * resource to do a static translation. Only an address range which will lie
 * in the range specified by MEM_VALIDATION will be successfully validated.
 *
 * If a more complex, or non-static validation is required, then the
 * implementor has the following options:
 * - Rewrite this function to provide such a validation
 * - Integrate the provider of the memory with UMP.
 *
 * @param phys_base page-aligned base address of the GA physical range to be
 * validated.
 *
 * @param size size of the address range to be validated, which must be a
 * multiple of the physical page size.
 *
 * @return _XYGPU_OSK_ERR_OK if the GA physical range is valid. Otherwise, a
 * suitable _xygpu_osk_errcode_t error.
 *
 */
_xygpu_osk_errcode_t xygpu_kernel_core_validate_xygpu_phys_range( u64 phys_base, u32 size );

#if USING_XYGPU_PMM
/**
 * @brief Signal a power up on a GA core.
 *
 * This function flags a core as powered up.
 * For PP and GP cores it calls functions that move the core from a power off
 * queue into the idle queue ready to run jobs. It also tries to schedule any
 * pending jobs to run on it.
 *
 * This function will fail if the core is not powered off - either running or
 * already idle.
 *
 * @param core The PMM core id to power up.
 * @param queue_only When XYGPU_TRUE only re-queue the core - do not reset.
 *
 * @return _XYGPU_OSK_ERR_OK if the core has been powered up. Otherwise a
 * suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_core_signal_power_up( xygpu_pmm_core_id core, xygpu_bool queue_only );

/**
 * @brief Signal a power down on a GA core.
 *
 * This function flags a core as powered down.
 * For PP and GP cores it calls functions that move the core from an idle
 * queue into the power off queue.
 *
 * This function will fail if the core is not idle - either running or
 * already powered down.
 *
 * @param core The PMM core id to power up.
 * @param immediate_only Do not set the core to pending power down if it can't
 * power down immediately
 *
 * @return _XYGPU_OSK_ERR_OK if the core has been powered up. Otherwise a
 * suitable _xygpu_osk_errcode_t error.
 */
_xygpu_osk_errcode_t xygpu_core_signal_power_down( xygpu_pmm_core_id core, xygpu_bool immediate_only );

#endif

/**
 * Flag to indicate whether or not xygpu_benchmark is turned on.
 */
extern int xygpu_benchmark;


#endif /* SRC_COMMON_XYGPU_KERNEL_CORE_H_ */
