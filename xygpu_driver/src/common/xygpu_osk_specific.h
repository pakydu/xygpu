/*
 * xygpu_osk_specific.h
 *
 *  Created on: Oct 17, 2022
 *      Author: paky
 */

#ifndef SRC_COMMON_XYGPU_OSK_SPECIFIC_H_
#define SRC_COMMON_XYGPU_OSK_SPECIFIC_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define XYGPU_STATIC_INLINE     static inline
#define XYGPU_NON_STATIC_INLINE inline

#ifdef __cplusplus
}
#endif


#endif /* SRC_COMMON_XYGPU_OSK_SPECIFIC_H_ */
