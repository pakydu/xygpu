/*
 * xygpu_covert2sylix.h
 *
 *  this file will define some special functions which defined in linux,
 *  try to find the sylix's implement for them.
 *  Created on: Oct 17, 2022
 *      Author: paky
 */

#ifndef SRC_COMMON_XYGPU_COVERT2SYLIX_H_
#define SRC_COMMON_XYGPU_COVERT2SYLIX_H_

#include <SylixOS.h>
//#include <module.h>
#include <linux/compat.h>

//Sylixos doesn't have the "ERESTARTSYS", we will use the "ERESTART"
#define ERESTARTSYS  ERESTART



//Sylixos use the same memory address for user and kernel.
#define put_user(x, ptr)             \
({                                   \
    int __pu_err = 0;                \
    typeof(*(ptr)) *__p = (ptr);     \
    *__p = x;                        \
    __pu_err;                        \
})

#define get_user(x, ptr)             \
({                                   \
    int __pu_err = 0;                \
    typeof(*(ptr)) *__p = (ptr);     \
    x = *__p;                        \
    __pu_err;                        \
})

/*
#define copy_to_user(x_user, y_kernel, len))               \
({                                                         \
    memcpy((void *)(x_user), (void *)(y_kernel), len);     \
})

#define copy_from_user(y_kernel, x_user, len))              \
({                                                          \
    memcpy((void *)(y_kernel), (void *)(x_user), len);      \
})
*/

#endif /* SRC_COMMON_XYGPU_COVERT2SYLIX_H_ */
