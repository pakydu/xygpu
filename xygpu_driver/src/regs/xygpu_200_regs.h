/*
 * xygpu_200_regs.h
 *
 *  Created on: Oct 17, 2022
 *      Author: paky
 */

#ifndef SRC_REGS_XYGPU_200_REGS_H_
#define SRC_REGS_XYGPU_200_REGS_H_


/**
 *  Enum for management register addresses.
 */
enum xygpu200_mgmt_reg
{
    XYGPU200_REG_ADDR_MGMT_VERSION                              = 0x1000,
    XYGPU200_REG_ADDR_MGMT_CURRENT_REND_LIST_ADDR               = 0x1004,
    XYGPU200_REG_ADDR_MGMT_STATUS                               = 0x1008,
    XYGPU200_REG_ADDR_MGMT_CTRL_MGMT                            = 0x100c,

    XYGPU200_REG_ADDR_MGMT_INT_RAWSTAT                          = 0x1020,
    XYGPU200_REG_ADDR_MGMT_INT_CLEAR                            = 0x1024,
    XYGPU200_REG_ADDR_MGMT_INT_MASK                             = 0x1028,
    XYGPU200_REG_ADDR_MGMT_INT_STATUS                           = 0x102c,

    XYGPU200_REG_ADDR_MGMT_WRITE_BOUNDARY_LOW                   = 0x1044,

    XYGPU200_REG_ADDR_MGMT_BUS_ERROR_STATUS                     = 0x1050,

    XYGPU200_REG_ADDR_MGMT_PERF_CNT_0_ENABLE                    = 0x1080,
    XYGPU200_REG_ADDR_MGMT_PERF_CNT_0_SRC                       = 0x1084,
    XYGPU200_REG_ADDR_MGMT_PERF_CNT_0_VALUE                     = 0x108c,

    XYGPU200_REG_ADDR_MGMT_PERF_CNT_1_ENABLE                    = 0x10a0,
    XYGPU200_REG_ADDR_MGMT_PERF_CNT_1_SRC                       = 0x10a4,
    XYGPU200_REG_ADDR_MGMT_PERF_CNT_1_VALUE                     = 0x10ac,

    XYGPU200_REG_SIZEOF_REGISTER_BANK                           = 0x10f0

};

#define XYGPU200_REG_VAL_PERF_CNT_ENABLE 1

enum xygpu200_mgmt_ctrl_mgmt {
    XYGPU200_REG_VAL_CTRL_MGMT_STOP_BUS         = (1<<0),
#if defined(USING_XYGPU200)
    XYGPU200_REG_VAL_CTRL_MGMT_FLUSH_CACHES     = (1<<3),
#endif
    XYGPU200_REG_VAL_CTRL_MGMT_FORCE_RESET      = (1<<5),
    XYGPU200_REG_VAL_CTRL_MGMT_START_RENDERING  = (1<<6),
#if defined(USING_XYGPU400)
    XYGPU400PP_REG_VAL_CTRL_MGMT_SOFT_RESET     = (1<<7),
#endif
};

enum xygpu200_mgmt_irq {
    XYGPU200_REG_VAL_IRQ_END_OF_FRAME          = (1<<0),
    XYGPU200_REG_VAL_IRQ_END_OF_TILE           = (1<<1),
    XYGPU200_REG_VAL_IRQ_HANG                  = (1<<2),
    XYGPU200_REG_VAL_IRQ_FORCE_HANG            = (1<<3),
    XYGPU200_REG_VAL_IRQ_BUS_ERROR             = (1<<4),
    XYGPU200_REG_VAL_IRQ_BUS_STOP              = (1<<5),
    XYGPU200_REG_VAL_IRQ_CNT_0_LIMIT           = (1<<6),
    XYGPU200_REG_VAL_IRQ_CNT_1_LIMIT           = (1<<7),
    XYGPU200_REG_VAL_IRQ_WRITE_BOUNDARY_ERROR  = (1<<8),
    XYGPU400PP_REG_VAL_IRQ_INVALID_PLIST_COMMAND = (1<<9),
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_UNDERFLOW  = (1<<10),
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_OVERFLOW   = (1<<11),
    XYGPU400PP_REG_VAL_IRQ_RESET_COMPLETED       = (1<<12),
};

#if defined USING_XYGPU200
#define XYGPU200_REG_VAL_IRQ_MASK_ALL  ((enum xygpu200_mgmt_irq) (\
    XYGPU200_REG_VAL_IRQ_END_OF_FRAME                           |\
    XYGPU200_REG_VAL_IRQ_END_OF_TILE                            |\
    XYGPU200_REG_VAL_IRQ_HANG                                   |\
    XYGPU200_REG_VAL_IRQ_FORCE_HANG                             |\
    XYGPU200_REG_VAL_IRQ_BUS_ERROR                              |\
    XYGPU200_REG_VAL_IRQ_BUS_STOP                               |\
    XYGPU200_REG_VAL_IRQ_CNT_0_LIMIT                            |\
    XYGPU200_REG_VAL_IRQ_CNT_1_LIMIT                            |\
    XYGPU200_REG_VAL_IRQ_WRITE_BOUNDARY_ERROR))
#elif defined USING_XYGPU400
#define XYGPU200_REG_VAL_IRQ_MASK_ALL  ((enum xygpu200_mgmt_irq) (\
    XYGPU200_REG_VAL_IRQ_END_OF_FRAME                           |\
    XYGPU200_REG_VAL_IRQ_END_OF_TILE                            |\
    XYGPU200_REG_VAL_IRQ_HANG                                   |\
    XYGPU200_REG_VAL_IRQ_FORCE_HANG                             |\
    XYGPU200_REG_VAL_IRQ_BUS_ERROR                              |\
    XYGPU200_REG_VAL_IRQ_BUS_STOP                               |\
    XYGPU200_REG_VAL_IRQ_CNT_0_LIMIT                            |\
    XYGPU200_REG_VAL_IRQ_CNT_1_LIMIT                            |\
    XYGPU200_REG_VAL_IRQ_WRITE_BOUNDARY_ERROR                   |\
    XYGPU400PP_REG_VAL_IRQ_INVALID_PLIST_COMMAND                  |\
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_UNDERFLOW                   |\
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_OVERFLOW                    |\
    XYGPU400PP_REG_VAL_IRQ_RESET_COMPLETED))
#else
#error "No supported xygpu core defined"
#endif

#if defined USING_XYGPU200
#define XYGPU200_REG_VAL_IRQ_MASK_USED ((enum xygpu200_mgmt_irq) (\
    XYGPU200_REG_VAL_IRQ_END_OF_FRAME                           |\
    XYGPU200_REG_VAL_IRQ_HANG                                   |\
    XYGPU200_REG_VAL_IRQ_FORCE_HANG                             |\
    XYGPU200_REG_VAL_IRQ_BUS_ERROR                              |\
    XYGPU200_REG_VAL_IRQ_WRITE_BOUNDARY_ERROR))
#elif defined USING_XYGPU400
#define XYGPU200_REG_VAL_IRQ_MASK_USED ((enum xygpu200_mgmt_irq) (\
    XYGPU200_REG_VAL_IRQ_END_OF_FRAME                           |\
    XYGPU200_REG_VAL_IRQ_HANG                                   |\
    XYGPU200_REG_VAL_IRQ_FORCE_HANG                             |\
    XYGPU200_REG_VAL_IRQ_BUS_ERROR                              |\
    XYGPU200_REG_VAL_IRQ_BUS_STOP                               |\
    XYGPU200_REG_VAL_IRQ_WRITE_BOUNDARY_ERROR                   |\
    XYGPU400PP_REG_VAL_IRQ_INVALID_PLIST_COMMAND                  |\
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_UNDERFLOW                   |\
    XYGPU400PP_REG_VAL_IRQ_CALL_STACK_OVERFLOW))
#else
#error "No supported xygpu core defined"
#endif

#define XYGPU200_REG_VAL_IRQ_MASK_NONE ((enum xygpu200_mgmt_irq)(0))

enum xygpu200_mgmt_status {
    XYGPU200_REG_VAL_STATUS_RENDERING_ACTIVE     = (1<<0),
    XYGPU200_REG_VAL_STATUS_BUS_STOPPED          = (1<<4),
};

enum xygpu200_render_unit
{
    XYGPU200_REG_ADDR_FRAME = 0x0000,
};

#if defined USING_XYGPU200
#define XYGPU200_NUM_REGS_FRAME ((0x04C/4)+1)
#elif defined USING_XYGPU400
#define XYGPU200_NUM_REGS_FRAME ((0x058/4)+1)
#else
#error "No supported xygpu core defined"
#endif

enum xygpu200_wb_unit {
    XYGPU200_REG_ADDR_WB0 = 0x0100,
    XYGPU200_REG_ADDR_WB1 = 0x0200,
    XYGPU200_REG_ADDR_WB2 = 0x0300
};

/** The number of registers in one single writeback unit */
#ifndef XYGPU200_NUM_REGS_WBx
#define XYGPU200_NUM_REGS_WBx ((0x02C/4)+1)
#endif

/* This should be in the top 16 bit of the version register of GA PP */
#if defined USING_XYGPU200
#define XYGPU_PP_PRODUCT_ID 0xC807
#elif defined USING_XYGPU400
#define XYGPU_PP_PRODUCT_ID 0xCD07
#else
#error "No supported xygpu core defined"
#endif


#endif /* SRC_REGS_XYGPU_200_REGS_H_ */
